package net.kimhou.uiautomatordemo1;

import android.support.test.uiautomator.UiAutomatorTestCase;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiSelector;

/**
 * Created by dk on 16/9/21.
 */
public class AutoClickTest extends UiAutomatorTestCase {
    public void testDemo() throws UiObjectNotFoundException {
        getUiDevice().pressHome();

//        豌豆荚
        UiObject appItem = new UiObject(new UiSelector().text("豌豆荚").className("android.widget.TextView"));
        if (appItem.exists() && appItem.isEnabled()) {
            appItem.click();
        }

        UiObject tv = new UiObject(new UiSelector().text("10 份惊喜，私藏定制").className("android.widget.TextView"));
        if (tv.exists() && tv.isEnabled()) {
            tv.click();
        }

        UiObject editText = new UiObject(new UiSelector().className("android.widget.EditText").index(0));
        if (editText.exists() && editText.isEnabled()) {
            editText.setText("界面");
            new UiObject(new UiSelector().className("android.widget.TextView").index(0)).clickAndWaitForNewWindow();
        }

        new UiObject(new UiSelector().text("安装").className("android.widget.TextView").instance(0)).clickAndWaitForNewWindow();

        UiObject btn = new UiObject(new UiSelector().text("Next").className("android.widget.Button"));
        btn.click();
        btn.click();

        UiObject btnInstall = new UiObject(new UiSelector().text("Install").className("android.widget.Button"));
        btnInstall.clickAndWaitForNewWindow();

        new UiObject(new UiSelector().text("Done").className("android.widget.Button")).click();

        getUiDevice().pressBack();
        getUiDevice().pressHome();

        new UiObject(new UiSelector().descriptionContains("Apps").className("android.widget.TextView")).clickAndWaitForNewWindow();
        UiDevice.getInstance().swipe(600, 400, 30, 400, 10);

        UiObject item = new UiObject(new UiSelector().descriptionContains("界面").className("android.widget.TextView"));
        item.dragTo(162, 122, 40);

        new UiObject(new UiSelector().text("OK").className("android.widget.Button")).click();



//        小米
        UiObject appItemXiaomi = new UiObject(new UiSelector().text("Xiaomi Market").className("android.widget.TextView"));
        if (appItemXiaomi.exists() && appItemXiaomi.isEnabled()) {
            appItemXiaomi.click();
        }
        new UiObject(new UiSelector().descriptionContains("Search").className("android.widget.TextView").index(0)).clickAndWaitForNewWindow();
        UiObject editTextXiaomi = new UiObject(new UiSelector().className("android.widget.EditText").index(1));
        if (editTextXiaomi.exists() && editTextXiaomi.isEnabled()) {
            editTextXiaomi.setText("澎湃");
            new UiObject(new UiSelector().text("澎湃").className("android.widget.TextView").index(0)).clickAndWaitForNewWindow();
        }

        new UiObject(new UiSelector().text("Download").className("android.widget.TextView").instance(0)).clickAndWaitForNewWindow();

        UiObject btnXiaomi = new UiObject(new UiSelector().text("Next").className("android.widget.Button"));
        btnXiaomi.click();
        UiObject btnInstallXiaomi = new UiObject(new UiSelector().text("Install").className("android.widget.Button"));
        btnInstallXiaomi.clickAndWaitForNewWindow();

        getUiDevice().pressBack();
        getUiDevice().pressHome();

        new UiObject(new UiSelector().descriptionContains("Apps").className("android.widget.TextView")).clickAndWaitForNewWindow();
//        UiDevice.getInstance().swipe(600, 400, 30, 400, 10);

        UiObject itemXiaomi = new UiObject(new UiSelector().descriptionContains("The Paper").className("android.widget.TextView"));
        itemXiaomi.dragTo(162, 122, 40);

        new UiObject(new UiSelector().text("OK").className("android.widget.Button")).click();
    }
}