package net.kimhou.uiautomatordemo1;

import android.test.ActivityInstrumentationTestCase2;
import android.test.TouchUtils;
import android.test.suitebuilder.annotation.MediumTest;
import android.widget.Button;

import org.junit.Test;

/**
 * Created by dk on 16/9/22.
 */
public class MainActivityTest extends ActivityInstrumentationTestCase2<MainActivity> {

    private MainActivity mainActivity;
    private Button tv1;

    public MainActivityTest() {
        super(MainActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();

        setActivityInitialTouchMode(true);

        mainActivity = getActivity();
        tv1 = (Button) mainActivity.findViewById(R.id.tv1);
        for (int i = 0; i < 10; i++) {
            tv1.callOnClick();
        }

    }

    @MediumTest
    public void testClick() {
        TouchUtils.clickView(this, tv1);
    }


    @Test
    public void onCreate() throws Exception {

    }

}